$(document).ready(function(){

	function is_touch_device() {
		return (('ontouchstart' in window)
		    || (navigator.MaxTouchPoints > 0)
		    || (navigator.msMaxTouchPoints > 0));
	}

	// SelectMenu
	if($('.default-select')[0]) {
		$('.default-select select').selectmenu({
			maxHeight: 20,
			open: function(){
				$(this).parent().addClass('opened');


				var self = $(this);

				setTimeout(function(){
					var menuList = self.parent().find('.ui-menu'),
					active = menuList.find('.ui-state-focus');
					if(active[0]) {
						menuList.scrollTop(active.position().top + menuList.scrollTop());
					}
				}, 50);
			},
			close: function(){
				$(this).parent().removeClass('opened');
			}
		});

		$('.default-select .ui-menu').addClass( "overflow" );
	};
	// END SelectMenu

	if($('.order-steps')[0]) {
		var actIndex = $('.number-list .active').index();

		$('.number-list li').each(function(){
			if($(this).index() < actIndex) $(this).addClass('active');
		})

		var stepActive = $('.order-steps .number-list .active').length-1;

		if(stepActive == 0) $('.steps-line .inner').css('width', '12%');
		if(stepActive == 1) $('.steps-line .inner').css('width', '50%');
		if(stepActive == 2) $('.steps-line .inner').css('width', '100%');
	}

	$('.tabs-menu li a').on('click', function(){
		var filterBlock = $(this).parents('.content-filter');

		var li = $(this).parent(),
			index = li.index();

			if(li.hasClass('active') && !filterBlock.hasClass('hidden')) return false;

		$('.tabs-menu li.active').removeClass('active');

		li.addClass('active');

		if(filterBlock.hasClass('hidden')) {
			filterBlock.removeClass('hidden');

			$('.tabs-content > li.active').removeClass('active');
			$('.tabs-content > li').eq(index).addClass('active');

			filterBlock.find('.tabs-content').fadeIn(2000, function(){
				
			})
		} else {
			$('.tabs-content > li.active').fadeOut(function(){
				$(this).removeClass('active');

				$('.tabs-content > li').eq(index).fadeIn(function(){
					$(this).addClass('active');
				});
			});
		}

		return false;
	});

	$('.tire-price .new-price').hover(function(){
		$(this).find('.tip').stop();

		if($(this).find('.tip')[0]) {
			$(this).find('.tip').fadeIn();
		}
	}, function(){
		$(this).find('.tip').stop();
		if($(this).find('.tip')[0]) {
			$(this).find('.tip').fadeOut();
		}
	});

	$('.question-head li').on('click', function(){
		var li = $(this),
			index = li.index();

		if(li.hasClass('active')) return;

		$('.question-forms>li').stop(true, true);

		$('.question-forms>li.active').fadeOut(function(){
			$('.question-head li').removeClass('active');
			$(this).removeClass('active');

			$('.question-forms>li').eq(index).fadeIn(function(){
				li.addClass('active');
				$(this).addClass('active');
			});
		})
	});

	$('.text-head li').on('click', function(){
		var li = $(this),
			index = li.index();

		if(li.hasClass('active')) return;

		$('.text-tabs>li').stop(true, true);

		$('.text-tabs>li.active').fadeOut(function(){
			$('.text-head li').removeClass('active');
			$(this).removeClass('active');

			$('.text-tabs>li').eq(index).fadeIn(function(){
				li.addClass('active');
				$(this).addClass('active');
			});
		})
	});

	// Main Menu
	enquire.register("screen and (max-width: 700px)", function() {

		$('.menu-button').on('click', function(){
			$('.main-menu').fadeToggle();

			return false;
		})

		$('.main-menu a').on('click', function(){
			var self = $(this);

			if(self.siblings('ul')[0]) {
				self.siblings('ul').slideToggle();

				return false;
			}
		});
	});

	enquire.register("screen and (min-width: 701px)", function() {
		$('.menu-button').off('click');
		$('.main-menu a').off('click');

		$('.main-menu').removeAttr('style');
		$('.main-menu ul').removeAttr('style');
	});
	// END Main Menu


	// Filter Responsive
	enquire.register("screen and (max-width: 1180px)", function() {
		$('.content-filter .tabs-content>li').each(function(){
			var tabText = $(this).find('.tab-text'),
				selectsWrap = $(this).find('.selects-wrap');
				tabText.after(selectsWrap);
		});
	});

	enquire.register("screen and (min-width: 1181px)", function() {
		$('.content-filter .tabs-content>li').each(function(){
			var tabText = $(this).find('.tab-text'),
				selectsWrap = $(this).find('.selects-wrap');
				tabText.before(selectsWrap);
		});
	});
	// END Filter Responsive

	// Fixed Basket Button
	$(document).on('scroll', function(){
		if($(document).scrollTop() > 150) {
			$('.header-top .basket-button').addClass('fixed');
		} else {
			$('.header-top .basket-button').removeClass('fixed');
		}
	});
	// Fixed Basket Button

})

$(window).load(function(){
	if($('.fancy-gallery')[0]) {
		$('.fancy-gallery').fancybox({
			padding:10,
			fitToView : false,
			wrapCSS: 'fancyclass'
		});
	}

	// if($('.works-gallery')[0]) {
	// 	$('.works-gallery').fancybox({padding:10});
	// }

	// if($('.flexslider.sponsors-slider')[0]) {
	// 	$('.flexslider.sponsors-slider').flexslider({
	// 		animation: "slide",
	// 		animationLoop: true,
	// 		itemWidth: 218,
	// 		itemMargin: 0,
	// 		prevText: '',
	// 		nextText: '',
	// 		controlNav: false, 
	// 		directionNav: true
	// 	});
	// };
  
})